package com.choucair.formacion.pageobjects;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.proteccion.com/wps/portal/proteccion/web/home/general/solicita-clave")
public class ProteccionValidationPage extends PageObject {
		
	// campo nombres del usuario
	@FindBy(xpath = "//input[@id='nombres']")
	public WebElementFacade inputNombres;
	
	// campo primer Apellido del usuario
	@FindBy(xpath = "//input[@id='primerApellido']")
	public WebElementFacade inputPrimerApellido;
	
	// campo segundo Apellido del usuario
	@FindBy(xpath = "//input[@id='segundoApellido']")
	public WebElementFacade inputSegundoApellido;
	
	// campo tipo de documento del usuario
	@FindBy(xpath = "//select[@id='tipoIdentificacion']")
	public WebElementFacade selectTipoDoc;
	
	// campo numero de documento de identidad del usuario
	@FindBy(xpath = "//input[@id='identificacion']")
	public WebElementFacade inputDocumento;
	
	//boton validad identidad del usuario
	@FindBy(xpath = "//a[text()='Validar mi identidad']")
	public WebElementFacade btnValidar;
	
	//...más ventajas
	@FindBy(xpath = "//span[text() = '...más ventajas']")
	public WebElementFacade btnVentajas;
	
	// texto en ...más ventajas
	@FindBy(xpath = "//ul[@class='listaPeq']//following::li[11]")
	public WebElementFacade liTexto;
	
	// popup error de fecha
	@FindBy(xpath = "//div[@id='popup_message']")
	public WebElementFacade divMensajeError;
	
	public void cambiarFrame() {
		getDriver().switchTo().frame("contenido");
		getDriver().switchTo().frame("contenido2");		
	}
	
	public void nombres(String nombres) {
		inputNombres.click();
		inputNombres.clear();
		inputNombres.sendKeys(nombres);
	}
	
	public void primerApellido(String primerApellido) {
		inputPrimerApellido.click();
		inputPrimerApellido.clear();
		inputPrimerApellido.sendKeys(primerApellido);
	}
	
	public void segundoApellido(String segundoApellido) {
		inputSegundoApellido.click();
		inputSegundoApellido.clear();
		inputSegundoApellido.sendKeys(segundoApellido);
	}
	
	public void selectTipoDoc(String tipoDoc) {
		selectTipoDoc.click();
		selectTipoDoc.selectByVisibleText(tipoDoc);
	}
	
	public void documento(String documento) {
		inputDocumento.click();
		inputDocumento.clear();
		inputDocumento.sendKeys(documento);
	}
	
	public void btnValidar() {
		btnValidar.click();
	}
	
	public void btnVentajas() {
		btnVentajas.click();
	}
	
	public void validarClikVentajas() {
		String texto = "Administre su cuenta individual de forma confidencial";
		String mensaje = liTexto.getText().trim();
		assertThat(mensaje, containsString(texto));
	}
	
	public void validarErrorFecha() {
		String texto = "- Debe ingresar una fecha de expedición";
		String mensaje = divMensajeError.getText().trim();
		System.out.println(mensaje);
	}
}
