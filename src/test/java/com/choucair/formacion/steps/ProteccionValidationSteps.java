package com.choucair.formacion.steps;

import java.io.FileReader;
import java.io.IOException;

import com.choucair.formacion.pageobjects.ProteccionValidationPage;

import au.com.bytecode.opencsv.CSVReader;
import net.thucydides.core.annotations.Step;

public class ProteccionValidationSteps {
	
	ProteccionValidationPage proteccionValidationPage;
	
	@Step
	public void ingresoPagina(){
		proteccionValidationPage.open();
		proteccionValidationPage.cambiarFrame();
	}
	
	@Step
	public void clickVentajas() {		
		proteccionValidationPage.btnVentajas();		
	}
	
	@Step
	public void validarVentajas(){		
		proteccionValidationPage.validarClikVentajas();
	}
	
	@Step
	public void llenarFormulario(int id) throws IOException {
		String csvFile = "src/test/resources/Datadriven/dataDriven.csv";
		CSVReader reader = null;
		reader = new CSVReader(new FileReader(csvFile));
		String[] cell = reader.readNext();
		
		while((cell = reader.readNext()) != null) {
			if(Integer.parseInt(cell[0]) == id) {
				proteccionValidationPage.nombres(cell[1]);
				proteccionValidationPage.primerApellido(cell[2]);
				proteccionValidationPage.segundoApellido(cell[3]);
				proteccionValidationPage.selectTipoDoc(cell[4]);
				proteccionValidationPage.documento(cell[5]);
			}
		}
		reader.close();
		proteccionValidationPage.btnValidar();
	}
		
	@Step
	public void validarErrorFecha() {
		proteccionValidationPage.validarErrorFecha();
	}
	
	
}
