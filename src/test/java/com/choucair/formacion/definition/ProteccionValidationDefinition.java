package com.choucair.formacion.definition;


import java.io.IOException;

import com.choucair.formacion.steps.ProteccionValidationSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ProteccionValidationDefinition {
	
	@Steps
	ProteccionValidationSteps proteccionValidationSteps;

	@Given("^Ingreso a la solicitud de clave$")
	public void ingreso_a_la_solicitud_de_clave(){
	    proteccionValidationSteps.ingresoPagina();
	}

	@Given("^Ingreso a al enlace mas ventajas$")
	public void ingreso_a_al_enlace_mas_ventajas(){
	    proteccionValidationSteps.clickVentajas();
	}

	@When("^Ingreso los datos del formulario validacion de identidad (\\d+)$")
	public void ingreso_los_datos_del_formulario_validacion_de_identidad(int id) {
	    try {
			proteccionValidationSteps.llenarFormulario(id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("hay problemas con el archivo csv\n"+e.getMessage());
		}
	}

	@Then("^Verifico mensaje error de fecha$")
	public void verifico_mensaje_error_de_fecha(){
	   proteccionValidationSteps.validarErrorFecha();
	}

	@Then("^Verifico texto en enlace mas ventajas$")
	public void verifico_texto_en_enlace_mas_ventajas(){
	    proteccionValidationSteps.validarVentajas();
	}

}
